    00. Foreword by Aslak Knutsen
    01. Foreword by Josh Long - Unit Testing in a Distributed World
    02. Preface
    03. About the author
    04. About the reviewers
    05. Book structure
    1. Foundations of testing
        1.1 Defining testing
        1.2 Unit Testing
        1.3 Integration Testing
        1.4 Other testing flavors
        1.5 Summary
    2. Developer testing tools
        2.1 A time before our time
        2.2 Life-cycle phases
        2.3 JUnit
        2.4 TestNG
        2.5 Summary
    3. Test-Friendly Design
        3.1 Reducing coupling
        3.2 Double definitions
        3.3 Mockito
        3.4 Ready to use Fakes
        3.5 Summary
    4. Automated testing
        4.1 Continuous integration
        4.2 Automated build tools
        4.3 Summary
    5. Infrastructure Resources Integration
        5.1 Common resource integration testing techniques
        5.2 System Time integration
        5.3 Filesystem integration
        5.4 Database integration
        5.5 eMail integration
        5.6 FTP integration
        5.7 Summary
    6. Testing Integration with Web Services
        6.1 REST Web Services
        6.2 SOAP Web Services
        6.3 Summary
    7. Spring in-container testing
        7.1 Spring dependencies management
        7.2 Spring configuration modularization
        7.3 Spring Test
        7.4 Test doubles into the Spring context
        7.5 Case study: Spring Pet clinic part 1
        7.6 Summary
    8. Spring Web MVC testing
        8.1 Context hierarchies
        8.2 Testing Spring MVC applications
        8.3 Case study: Spring Pet Clinic part 2
        8.4 Summary
    9. Java EE testing
        9.1 Shrinkwrap
        9.2 Arquillian
        9.3 Summary
    10. Appendix A: Tools
        10.1 Root frameworks
        10.1 Assertion frameworks
        10.2 Mocking frameworks
        10.3 Fake API frameworks
        10.4 Fake Resource frameworks
        10.5 In-container related frameworks
        10.6 Miscellaneous libraries

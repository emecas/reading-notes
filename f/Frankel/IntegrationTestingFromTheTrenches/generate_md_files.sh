while read line; do  
	newline=`echo $line | tr -cd "[:print:]"`
	newline=`echo $newline | tr -s "[:punct:]" "_"`
	newline=`echo $newline | tr -s "[:blank:]" "-"`
	touch "${newline}.md"
done < TOC.txt
